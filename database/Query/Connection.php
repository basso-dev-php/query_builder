<?php

namespace QueryBuilder\Query;

use PDO;
use PDOException;

class Connection
{
    private static $pdo;
    private static $dns;
    private static $user;
    private static $password;

    /**
     * Connect to database.
     *
     * @return PDO
     */
    public static function connect(): PDO
    {
        if (!self::$pdo) {
            try {
                self::setConfigDataBase();
                self::$pdo = new PDO(self::$dns, self::$user, self::$password);
                return self::$pdo;
            } catch (PDOException $e) {
                echo "CodeError: {$e->getCode()} <br> MessageError: {$e->getMessage()}";
                die;
            }
        }
    }

    /**
     * Set driver, host, port, password and database connection user
     *
     * @return void
     */
    private static function setConfigDataBase(): void
    {
        self::$dns = getenv('DB_CONFIG_DRIVER')
        . ':host=' . getenv('DB_CONFIG_HOST')
        . ';dbname=' . getenv('DB_CONFIG_DBNAME');

        self::$password = getenv('DB_CONFIG_PASSWORD');
        self::$user = getenv('DB_CONFIG_USER');
    }
    public static function close()
    {
    }
}
